package com.example.lifecycle;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;


public class MainActivity extends AppCompatActivity {

    TextView tv_counter;
    Button b_clicker;
    int count = 0;


    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt("countValue", count);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        count = savedInstanceState.getInt("countValue");
        tv_counter.findViewById(R.id.tv_counter);
        tv_counter.setText(Integer.toString(count));
    }

    @Override
    protected void onStart() {
        super.onStart();
        Log.d("LifeCycle", "The app has started.");
    }

    @Override
    protected void onStop() {
        super.onStop();
        Log.d("LifeCycle", "The app has stopped.");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.d("LifeCycle", "The app has been destroyed.");
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.d("LifeCycle", "The app has been paused.");
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        Log.d("LifeCycle", "The app has restarted.");
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d("LifeCycle", "The app has been created.");
        setContentView(R.layout.activity_main);

        tv_counter = findViewById(R.id.tv_counter);
        b_clicker = findViewById(R.id.b_clicker);

        b_clicker.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                count++;
                tv_counter.setText(Integer.toString(count));
            }
        });
    }
}
